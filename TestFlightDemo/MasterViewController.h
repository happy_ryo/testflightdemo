//
//  MasterViewController.h
//  TestFlightDemo
//
//  Created by 岩間 亮 on 12/03/16.
//  Copyright (c) 2012 jp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
