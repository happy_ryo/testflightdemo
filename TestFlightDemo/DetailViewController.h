//
//  DetailViewController.h
//  TestFlightDemo
//
//  Created by 岩間 亮 on 12/03/16.
//  Copyright (c) 2012 jp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
