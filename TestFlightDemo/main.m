//
//  main.m
//  TestFlightDemo
//
//  Created by 岩間 亮 on 12/03/16.
//  Copyright (c) 2012 jp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
