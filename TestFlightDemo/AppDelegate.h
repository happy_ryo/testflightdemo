//
//  AppDelegate.h
//  TestFlightDemo
//
//  Created by 岩間 亮 on 12/03/16.
//  Copyright (c) 2012 jp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
